package modelo;

import static org.junit.jupiter.api.Assertions.*;

class NodoTest {

    @org.junit.jupiter.api.Test
    void calcularHeuristica() {
        Nodo nodoTest = new Nodo(3,5);
        Nodo nodoFinal = new Nodo(7,7);
        nodoTest.setH(6);
        assertEquals(nodoTest.getH(),nodoTest.calcularHeuristica(nodoFinal));
    }

    @org.junit.jupiter.api.Test
    void establecerDatosNodo() {
        Nodo nodoTest = new Nodo(3,5);
        Nodo nodoActual = new Nodo(7,7);
        nodoTest.establecerDatosNodo(nodoActual,6);
        assertEquals(nodoActual,nodoTest.getPadre());
        assertEquals(6,nodoTest.getG());
        assertEquals(6,nodoTest.getF());
    }

    @org.junit.jupiter.api.Test
    void comprobarMejorCamino() {
        Nodo nodoTest = new Nodo(5,5);
        Nodo nodoVecino = new Nodo(6,5);

        nodoTest.setG(24);
        nodoVecino.setG(10);
        assertTrue(nodoTest.comprobarMejorCamino(nodoVecino,10));
    }

    @org.junit.jupiter.api.Test
    void calcularF() {
        Nodo nodoActual = new Nodo(0,0);
        nodoActual.setG(10);
        nodoActual.setH(14);
        nodoActual.setF(24);
        assertEquals(nodoActual.getF(),nodoActual.calcularF());
    }
}