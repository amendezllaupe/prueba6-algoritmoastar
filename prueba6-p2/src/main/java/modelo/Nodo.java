package modelo;

public class Nodo {
    private int g;
    private int h;
    private int f;
    private int fila;
    private int columna;
    private Nodo padre;
    private boolean esObstaculo;

    public Nodo(int fila, int columna) {
        this.fila = fila;
        this.columna = columna;
    }

    public int calcularHeuristica(Nodo nodoFinal){
        return Math.abs(nodoFinal.getFila() - this.getFila()) + Math.abs(nodoFinal.getColumna() - this.getColumna());
    }

    public void establecerDatosNodo(Nodo nodoActual, int costo){
        int g = nodoActual.getG() + costo;
        this.setPadre(nodoActual);
        this.setG(g);
        this.setF(calcularF());
    }

    public boolean comprobarMejorCamino(Nodo nodoActual, int costo){
        int g = nodoActual.getG() + costo;
        if (g<this.getG()){
            establecerDatosNodo(nodoActual,costo);
            return true;
        }
        return false;
    }

    public int calcularF(){
        return getG() + getH();
    }

    public boolean esObstaculo() {
        return esObstaculo;
    }

    public void setObstaculo(boolean esObstaculo) {
        this.esObstaculo = esObstaculo;
    }

    public int getFila() {
        return fila;
    }

    public void setFila(int fila) {
        this.fila = fila;
    }

    public int getColumna() {
        return columna;
    }

    public void setColumna(int columna) {
        this.columna = columna;
    }

    public int getG() {
        return g;
    }

    public void setG(int g) {
        this.g = g;
    }

    public int getH() {
        return h;
    }

    public void setH(int h) {
        this.h = h;
    }

    public int getF() {
        return f;
    }

    public void setF(int f) {
        this.f = f;
    }

    public Nodo getPadre() {
        return padre;
    }

    public void setPadre(Nodo padre) {
        this.padre = padre;
    }
}

