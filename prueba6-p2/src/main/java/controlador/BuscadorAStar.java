package controlador;

import modelo.Nodo;

import java.util.*;

public class BuscadorAStar {
    private static int costoMovRecto = 10;
    private static int costoMovDiagonal = 14;
    private int costoHV;
    private int costoDiagonal;
    private Mapa mapa;
    private PriorityQueue<Nodo> listaAbierta;
    private Set<Nodo> conjuntoCerrado;
    private Nodo nodoInicial;
    private Nodo nodoFinal;

    public BuscadorAStar(int filas, int columnas, Nodo nodoInicial, Nodo nodoFinal, int costoHV, int costoDiagonal) {
        this.costoHV = costoHV;
        this.costoDiagonal = costoDiagonal;
        setNodoInicial(nodoInicial);
        setNodoFinal(nodoFinal);
        this.mapa = new Mapa(filas,columnas);
        this.listaAbierta = new PriorityQueue<Nodo>(new Comparator<Nodo>() {
            @Override
            public int compare(Nodo nodo1, Nodo nodo2) {
                return Integer.compare(nodo1.getF(), nodo2.getF());
            }
        });
        mapa.definirNodos(nodoFinal);
        this.conjuntoCerrado = new HashSet<>();
    }

    public BuscadorAStar(int filas,int columnas,Nodo nodoInicial, Nodo nodoFinal){
        this(filas,columnas,nodoInicial,nodoFinal,costoMovRecto,costoMovDiagonal);
    }

    public List<Nodo> buscarCamino(){
        listaAbierta.add(nodoInicial);
        while(!listaAbierta.isEmpty()){
            Nodo nodoActual = listaAbierta.poll();
            conjuntoCerrado.add(nodoActual);
            if (nodoActual.equals(nodoFinal)){
                return obtenerCamino(nodoActual);
            } else {
                agregarVecinos(nodoActual);
            }
        }
        return new ArrayList<Nodo>();
    }

    public List<Nodo> obtenerCamino(Nodo nodoActual){
        List<Nodo> ruta = new ArrayList<Nodo>();
        ruta.add(nodoActual);
        Nodo padre;
        while ((padre = nodoActual.getPadre())!=null){
            ruta.add(0,padre);
            nodoActual = padre;
        }
        return ruta;
    }

    private void agregarVecinos(Nodo nodoActual){
        agregarVecinoArriba(nodoActual);
        agregarVecinoLado(nodoActual);
        agregarVecinoAbajo(nodoActual);
    }

    private void agregarVecinoAbajo(Nodo nodoActual){
        int fila = nodoActual.getFila();
        int columna = nodoActual.getColumna();
        int filaAbajo = fila + 1;
        if (filaAbajo<mapa.getAreaBusqueda().length){
            if (columna-1 >= 0) revisarNodo(nodoActual,columna-1,filaAbajo, getCostoDiagonal());
            if (columna+1 < mapa.getAreaBusqueda()[0].length) revisarNodo(nodoActual, columna+1, filaAbajo, getCostoDiagonal());
            revisarNodo(nodoActual,columna,filaAbajo,getCostoHV());
        }
    }

    private void agregarVecinoLado(Nodo nodoActual){
        int fila = nodoActual.getFila();
        int columna = nodoActual.getColumna();
        int filaLado = fila;
        if (columna-1 >= 0) revisarNodo(nodoActual,columna-1,filaLado,getCostoHV());
        if (columna+1 < mapa.getAreaBusqueda()[0].length) revisarNodo(nodoActual,columna+1, filaLado,getCostoHV());
    }

    private void agregarVecinoArriba(Nodo nodoActual){
        int fila = nodoActual.getFila();
        int columna = nodoActual.getColumna();
        int filaArriba = fila + 1;
        if (filaArriba >= 0){
            if (columna-1 >= 0) revisarNodo(nodoActual,columna-1,filaArriba, getCostoDiagonal());
            if (columna+1 < mapa.getAreaBusqueda()[0].length) revisarNodo(nodoActual, columna+1, filaArriba, getCostoDiagonal());
            revisarNodo(nodoActual,columna,filaArriba,getCostoHV());
        }
    }

    private void revisarNodo(Nodo nodoActual,int columna,int fila, int costo){
        Nodo nodoVecino = mapa.getAreaBusqueda()[fila][columna];
        if (!nodoVecino.esObstaculo() && !getConjuntoCerrado().contains(nodoVecino)){
            if (!getListaAbierta().contains(nodoVecino)){
                nodoVecino.establecerDatosNodo(nodoActual,costo);
            } else {
                boolean cambio = nodoVecino.comprobarMejorCamino(nodoActual,costo);
                if (cambio){
                    getListaAbierta().remove(nodoVecino);
                    getListaAbierta().add(nodoVecino);
                }
            }
        }
    }

    public Mapa getMapa() {
        return mapa;
    }

    public void setMapa(Mapa mapa) {
        this.mapa = mapa;
    }

    public int getCostoHV() {
        return costoHV;
    }

    public void setCostoHV(int costoHV) {
        this.costoHV = costoHV;
    }

    public int getCostoDiagonal() {
        return costoDiagonal;
    }

    public void setCostoDiagonal(int costoDiagonal) {
        this.costoDiagonal = costoDiagonal;
    }

    public PriorityQueue<Nodo> getListaAbierta() {
        return listaAbierta;
    }

    public void setListaAbierta(PriorityQueue<Nodo> listaAbierta) {
        this.listaAbierta = listaAbierta;
    }

    public Set<Nodo> getConjuntoCerrado() {
        return conjuntoCerrado;
    }

    public void setConjuntoCerrado(Set<Nodo> conjuntoCerrado) {
        this.conjuntoCerrado = conjuntoCerrado;
    }

    public Nodo getNodoInicial() {
        return nodoInicial;
    }

    public void setNodoInicial(Nodo nodoInicial) {
        this.nodoInicial = nodoInicial;
    }

    public Nodo getNodoFinal() {
        return nodoFinal;
    }

    public void setNodoFinal(Nodo nodoFinal) {
        this.nodoFinal = nodoFinal;
    }
}
