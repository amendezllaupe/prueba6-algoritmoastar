package controlador;

import modelo.Nodo;

public class Mapa {

    private Nodo[][] areaBusqueda;

    public Mapa(int filas, int columnas){
        this.areaBusqueda = new Nodo[filas][columnas];
    }

    public void definirNodos(Nodo nodoFinal){
        for (int i = 0; i < areaBusqueda.length; i++) {
            for (int j = 0; j < areaBusqueda[0].length; j++) {
                Nodo nodoActual = new Nodo(i,j);
                nodoActual.setH(nodoActual.calcularHeuristica(nodoFinal));
                this.areaBusqueda[i][j] = nodoActual;
            }
        }
    }

    public void generarObstaculos(int[][] obstaculos){
        for (int i = 0; i < obstaculos.length; i++) {
            int fila = obstaculos[i][0];
            int columna = obstaculos[i][1];
            this.areaBusqueda[fila][columna].setObstaculo(true);
        }
    }

    public Nodo[][] getAreaBusqueda() {
        return areaBusqueda;
    }

    public void setAreaBusqueda(Nodo[][] areaBusqueda) {
        this.areaBusqueda = areaBusqueda;
    }

}
